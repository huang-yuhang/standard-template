/*
 * @Description: 
 * @Author: HYH
 * @LastEditors: HYH
 * @LastEditTime: 2023-03-31 13:36:43
 */
import { lazy, } from 'react'
import { Appraisal, lazyLoad } from '..'

const BasicUse = lazy(() => import('@/views/fabric/basicUse'))
const Custom = lazy(() => import('@/views/fabric/custom'))
const Test = lazy(() => import('@/views/fabric/test'))

export default () => {
  return {
    path: 'fabric',
    element: <Appraisal>{lazyLoad('fabric')}</Appraisal>,
    children: [
      {
        path: 'basicUse',
        element: <BasicUse />,
        meta: {
          title: '基本使用',
          parent: 'fabric'
        }
      },
      {
        path: 'custom',
        element: <Custom />,
        meta: {
          title: '自定义',
          parent: 'fabric'
        }
      },
      {
        path: 'test',
        element: <Test />,
        meta: {
          title: '考试系统',
          parent: 'fabric'
        }
      },
    ]
  }
}