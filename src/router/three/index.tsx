/*
 * @Description:
 * @Author: HYH
 * @LastEditors: HYH
 * @LastEditTime: 2023-03-29 18:21:41
 */
import { lazy } from 'react'

const Base = lazy(() => import('@/views/three/components/base'))
const Cube = lazy(() => import('@/views/three/components/cube'))

import { Appraisal, lazyLoad } from '..'

export default () => {
  return {
    path: 'three',
    element: <Appraisal>{lazyLoad('three')}</Appraisal>,
    children: [
      {
        path: 'base',
        element: <Base />,
        meta: {
          title: 'base',
          parent: 'three'
        }
      },
      {
        path: 'cube',
        element: <Cube />,
        meta: {
          title: '几何体',
          parent: 'three'
        }
      }
    ]
  }
}
