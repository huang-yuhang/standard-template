/*
* @Description: 
* @Author: HYH
 * @LastEditors: HYH
 * @LastEditTime: 2023-03-29 16:26:12
*/
import { lazy, } from 'react'
import { Appraisal, lazyLoad } from '..'

const AudioRecord = lazy(() => import('@/views/usefulTools/audioRecord'))
const ScreenRecord = lazy(() => import('@/views/usefulTools/screenRecord'))


export default () => {
  return {
    path: 'usefulTools',
    element: <Appraisal>{lazyLoad('usefulTools')}</Appraisal>,
    meta: {
      title: '实用工具',
    },
    children: [
      {
        path: 'audioRecord',
        element: <AudioRecord />,
        meta: {
          title: '录音',
          parent: 'usefulTools'
        }
      },
      {
        path: 'screenRecord',
        element: <ScreenRecord />,
        meta: {
          title: '录屏',
          parent: 'usefulTools'
        }
      },
    ]
  }
}