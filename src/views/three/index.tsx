/*
 * @Description: 
 * @Author: HYH
 * @LastEditors: HYH
 * @LastEditTime: 2023-03-29 18:21:06
 */
import { useEffect, useState } from 'react';
import { Outlet, useRoutes } from 'react-router-dom'
import ThreeRoute from '@/router/three';
export default function Three() {


  return (
    <div id="base" style={{ width: '300px', height: '300px' }}>
      <Outlet />
    </div>
  )
}
