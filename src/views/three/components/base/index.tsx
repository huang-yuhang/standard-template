/*
 * @Description:
 * @Author: HYH
 * @LastEditors: HYH
 * @LastEditTime: 2023-03-29 17:59:12
 */
import React, { useEffect } from 'react'
import {
  AxesHelper,
  BoxGeometry,
  Mesh,
  MeshBasicMaterial,
  PerspectiveCamera,
  Scene,
  WebGL1Renderer
} from 'three'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls'
import gsap from 'gsap'
import { GUI } from 'dat.gui'

export default function Base() {
  useEffect(() => {
    const scene = new Scene()
    const camera = new PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000)
    camera.position.set(0, 0, 10)
    scene.add(camera)

    const cubeGeometry = new BoxGeometry(1, 1, 1)
    const cubeMaterial = new MeshBasicMaterial({ color: '#eee' })
    const cube = new Mesh(cubeGeometry, cubeMaterial)
    scene.add(cube)

    const renderer = new WebGL1Renderer()
    renderer.setSize(500, 500)
    const controls = new OrbitControls(camera, renderer.domElement)
    controls.enableDamping = true
    const axesHelper = new AxesHelper(5)
    scene.add(axesHelper)

    document.querySelector(`#base`)!.append(renderer.domElement)

    gsap.to(cube.position, { x: 5, duration: 3, repeat: -1, yoyo: true, ease: 'bounce.in' })
    gsap.to(cube.position, {
      x: 5 * Math.PI,
      duration: 3,
      repeat: -1,
      yoyo: true,
      ease: 'bounce.in'
    })

    const gui = new GUI()

    // @ts-ignore
    gui.add(cube.position, 'x').min(0).max(5).step(0.1).name('X轴')

    // @ts-ignore
    gui.add(cube.position, 'y').min(0).max(5).step(0.1).name('Y轴')

    // @ts-ignore
    gui.add(cube, 'visible').name('是否显示')

    gui.addColor({ color: '#eee' }, 'color').onChange(color => cube.material.color.set(color))

    const folder = gui.addFolder('设置')

    // @ts-ignore
    folder.add(cube.material, 'wireframe').name('是否显示网格')

    renderer.domElement.addEventListener('dblclick', () => {
      if (document.fullscreenElement) {
        document.exitFullscreen()
      } else {
        renderer.domElement.requestFullscreen()
      }
    })

    function render() {
      renderer.render(scene, camera)
      requestAnimationFrame(render)
    }
    render()
  }, [])

  return <div id="base"></div>
}
