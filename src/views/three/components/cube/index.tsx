/*
 * @Description:
 * @Author: HYH
 * @LastEditors: HYH
 * @LastEditTime: 2023-03-29 17:59:12
 */
import React, { useEffect } from 'react'
import {
  AxesHelper,
  BoxGeometry,
  Mesh,
  MeshBasicMaterial,
  PerspectiveCamera,
  Scene,
  WebGL1Renderer,
  TextureLoader
} from 'three'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls'
import gsap from 'gsap'
import { GUI } from 'dat.gui'

export default function Cube() {
  useEffect(() => {
    const scene = new Scene()
    const camera = new PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000)
    camera.position.set(0, 0, 10)
    scene.add(camera)

    const texture = new TextureLoader()
    const flowerTexture = texture.load('src/assets/img/flower.jpg')

    const cubeGeometry = new BoxGeometry(1, 1, 1)
    const cubeMaterial = new MeshBasicMaterial({ color: '#eee', map: flowerTexture })
    const cube = new Mesh(cubeGeometry, cubeMaterial)
    scene.add(cube)

    const renderer = new WebGL1Renderer()
    renderer.setSize(500, 500)
    const controls = new OrbitControls(camera, renderer.domElement)
    controls.enableDamping = true
    const axesHelper = new AxesHelper(5)
    scene.add(axesHelper)

    document.querySelector(`#base`)!.append(renderer.domElement)

    function render() {
      renderer.render(scene, camera)
      requestAnimationFrame(render)
    }
    render()

    renderer.domElement.addEventListener('dblclick', () => {
      if (document.fullscreenElement) {
        document.exitFullscreen()
      } else {
        renderer.domElement.requestFullscreen()
      }
    })
  }, [])

  return <div id="base"></div>
}
