/*
 * @Description: 
 * @Author: HYH
 * @LastEditors: HYH
 * @LastEditTime: 2023-03-22 18:11:28
 */
import { Outlet } from 'react-router-dom'

export default function Three() {

  return <Outlet />

}
