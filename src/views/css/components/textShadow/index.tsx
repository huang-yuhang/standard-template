/*
 * @Description: 
 * @Author: HYH
 * @LastEditors: HYH
 * @LastEditTime: 2023-03-29 14:15:01
 */
import CodeDetail from "@/components/codeDetail";
import styles from "./index.module.scss";
import { useCssData } from "@/hooks/useCss";

const TextShadow = () => {
  const { contents } = useCssData()

  const text = '这是一段文字'


  return (
    <CodeDetail contents={contents}>
      <div className={styles.container}>
        <div className={styles.content} data-text={text}>
          {text}
        </div>
      </div>
    </CodeDetail>

  );
};

export default TextShadow;
