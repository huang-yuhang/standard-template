/*
 * @Description: 
 * @Author: HYH
 * @LastEditors: HYH
 * @LastEditTime: 2023-03-29 14:02:08
 */
import flower from "@/assets/img/flower.jpg";
import CodeDetail from "@/components/codeDetail";
import styles from "./index.module.scss";
import { useCssData } from "@/hooks/useCss";
const Filter = () => {
  const { contents } = useCssData()

  return (
    <CodeDetail contents={contents}>
      <div className={styles.container}>
        <div className={styles.left}>
          <span>
            变亮
          </span>
          <img src={flower} alt="" />
        </div>
        <div className={styles.right}>
          <span>
            变灰
          </span>
          <img src={flower} alt="" />
        </div>
      </div>
    </CodeDetail>

  );
};

export default Filter;
