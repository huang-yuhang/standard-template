/*
 * @Description: 
 * @Author: HYH
 * @LastEditors: HYH
 * @LastEditTime: 2023-03-29 10:18:04
 */
import { useRef, useEffect } from "react";
import Typed from "typed.js";
import { Button } from "antd";
import styles from "./index.module.scss";
import CodeDetail from "@/components/codeDetail";
import { useCssData } from "@/hooks/useCss";


const Print = () => {
  const { contents } = useCssData()
  const pRef = useRef<HTMLParagraphElement>(null);
  const typed = useRef<any>(null);

  const onceAgain = () => {
    typed.current?.reset();
    typed.current?.start();
  };

  useEffect(() => {
    const options = {
      strings: ["Legends never die", "But you", "except"],
      typeSpeed: 70,
      showCursor: false,
    };

    if (pRef.current) {
      typed.current = new Typed(pRef.current, options);
    }

    return () => {
      typed.current?.destroy();
    };
  }, []);

  return (
    <CodeDetail contents={contents} >
      <div className={styles.container}>
        <p ref={pRef} />
        <Button type="primary" className={styles.btn} onClick={onceAgain}>
          Once Again
        </Button>
      </div>
    </CodeDetail>
  );
};

export default Print;
