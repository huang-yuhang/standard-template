/*
 * @Description: 
 * @Author: HYH
 * @LastEditors: HYH
 * @LastEditTime: 2023-04-04 16:19:48
 */
import CodeDetail from "@/components/codeDetail";
import style from "./index.module.scss";
import { useCssData } from "@/hooks/useCss";



export default () => {
  const { contents } = useCssData()

  return (
    <CodeDetail contents={contents} >
      <div className={style.buttons}>
        <button className={style.customBtn + ' ' + style.btn}>Read More</button>
      </div>
    </CodeDetail>


  )
}

