import CodeDetail from "@/components/codeDetail";
import styles from "./index.module.scss";
import { useCssData } from "@/hooks/useCss";

export default () => {
  const { contents } = useCssData()

  return (
    <CodeDetail contents={contents} >
      <div className={styles.container}>
        <div className={styles.content}></div>
      </div>
    </CodeDetail>
  );
};