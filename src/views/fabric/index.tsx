/*
 * @Description: 
 * @Author: HYH
 * @LastEditors: HYH
 * @LastEditTime: 2023-03-31 10:21:58
 */
import { Outlet } from "react-router-dom";


export default () => <Outlet />