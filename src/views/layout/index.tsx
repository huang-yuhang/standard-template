/*
 * @Description: 
 * @Author: HYH
 * @LastEditors: HYH
 * @LastEditTime: 2023-03-29 09:59:57
 */

import { Layout as AntLayout, } from 'antd';
const { Content, } = AntLayout;
import { Outlet, useRoutes, } from 'react-router-dom'
import routes from '@/router';
import Sider from './components/Sider';

export default function Layout() {

  return (
    <AntLayout style={{ height: '100vh' }}>
      <Sider />
      <Content style={{ overflow: "auto" }}>
        {useRoutes(routes)}
        <Outlet />
      </Content>
    </AntLayout>
  )
}
