/*
 * @Description: 
 * @Author: HYH
 * @LastEditors: HYH
 * @LastEditTime: 2023-03-22 14:06:29
 */
import { useEffect, useState } from 'react';
import { Layout, Menu, Switch, } from 'antd';
import { useMenu } from '@/hooks/useMenu';
import { useLocation, useNavigate, Navigate } from 'react-router-dom'
import { reduxActions, useAppDispatch, useAppSelector } from '@/store';
const { Sider } = Layout;


export default function Side() {
  const useDispatch = useAppDispatch()
  const navigate = useNavigate()
  const store = useAppSelector(state => state)

  const [collapsed, setCollapsed] = useState(store.collapsed);
  const [theme, setTheme] = useState(store.theme);

  const [openKeys,] = useState<string[]>([]);
  const { pathname } = useLocation()

  useEffect(() => {

  }, [])

  return (
    <Sider theme={theme} collapsible collapsed={collapsed} onCollapse={(value) => {
      setCollapsed(value)
      useDispatch(reduxActions.setCollapsed(!collapsed))
    }}>
      {/* 切换主题 */}
      <div style={{ height: 32, margin: 16, display: 'flex', alignItems: 'center' }} >
        <Switch
          onChange={(boolean) => {
            setTheme(boolean ? 'dark' : 'light')
            useDispatch(reduxActions.setTheme(boolean ? 'dark' : 'light'))
          }}
          checkedChildren="暗色" unCheckedChildren="亮色" defaultChecked />
      </div>
      <Menu
        onClick={(e) => {
          navigate(e.key, { replace: true })
        }}
        defaultSelectedKeys={[pathname]}
        defaultOpenKeys={openKeys}
        onOpenChange={(e) => {
          // console.log(e);
        }}
        style={{ height: '100%' }}
        theme={theme}
        mode="inline"
        items={useMenu()} />
    </Sider>
  )
}
