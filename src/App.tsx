/*
 * @Description: 
 * @Author: HYH
 * @LastEditors: HYH
 * @LastEditTime: 2023-03-29 10:09:10
 */

import './assets/base.css'
import Layout from '@/views/layout';
import { useCss } from '@/hooks/useCss';

const App = () => {
  useCss()
  return <Layout />
}

export default App;